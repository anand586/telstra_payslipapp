Short summary
Console Employee Payslip App - This App generates Monthly Income of a perticular employee based on Gross annual income given as input.
The App is designed to scale in near future based on Tax slab requirements changes. Tax slabs are under government control and they can vary any time depending on the environmental and political circumstanecs for financial year.
This App is designed in such a way that it should handle any such future circumstance of Tax slab changes and calculation without having to modifying core business logic. The application is loosly coupled and follows 
the SOLID principles to make App reliable and scalable. Uses Factory Design pattern to make the app flexible enough to accomudate future taxslabs changes without depending on modifying core logic and exposing business logic to client.
Test cases have been covered using Microsoft Nunit Framewok to make sure business logic does its work accuratly all the time
MYOB Payslip App is flexible and open for future modifications and there is always room for improvements

Tech Stack Used
1 .Netcore 3.0 
2 N Layer Architecture
3 Factory Design Pattern
4 SOLID Principles
5 NUnit Test Framework 


Instructions to Run the App
1. Open Solution in VS2019
2. 'Telstra_EmployeePayslipApp' is the main Console App that is client which talks to factory
3. 'BusinessLogicLayer' is the core logic which is called when Payslip is requested from client. This request to business logic layer is communicated via Factory. Keeping SOLID principles followed and client will not be able to 
view core logic and access. Business logic layer is INTERFACE DRIVEN and SUBCLASSES DECIDE which class to instantiate based on request
4. 5 NUnit Test cases framwork is used to cover logic to make sure calculation is accuratly working

Author
Anand Kulkarni
anandkulkarni586@gmail.com
0415837157
