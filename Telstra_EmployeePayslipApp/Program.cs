﻿using BusinessLogicLayer.Factory;
using BusinessLogicLayer.Interfaces;
using DataModelLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace Telstra_EmployeePayslipApp
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            var slabType = string.Empty;
            string grossAnnualSalary = string.Empty;
            string name = string.Empty;
            decimal annualSalary = 0.00m;
            TaxSlabFactoryAbstract calculateMonthlyIncome = new TaxSlabConcrete();
            CalculateTaxFactory calculate = new CalculateTaxFactory();
            PayslipModel model = new PayslipModel();
            IMonthlyIncome instance = null;

            Console.WriteLine("Employee Payslip Generator App!");

            Console.WriteLine("What is the Employee Annual Salary? Please Enter");
            grossAnnualSalary = Console.ReadLine();

            Console.WriteLine("What is the Employee Name? Please Enter");
            name = Console.ReadLine();

            annualSalary = Convert.ToDecimal(grossAnnualSalary);
            slabType = calculate.getTaxSlabType(annualSalary);

            instance = calculateMonthlyIncome.GetMonthlyIncome(slabType);

            var result = instance.getMonthlyIncome(annualSalary);
            Console.WriteLine("Employee Name : " + name);
            Console.WriteLine("Employee Annual Salary : " + result.AnnualSalary);
            Console.WriteLine("Employee Monthly Tax : " + Decimal.Round(result.MonthlyTax));
            Console.WriteLine("Monthly Salary : " + Decimal.Round(result.MonthlySalary));
            Console.ReadLine();
        }
    }
}
