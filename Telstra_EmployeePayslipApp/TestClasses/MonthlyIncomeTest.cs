﻿using BusinessLogicLayer.Factory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Telstra_EmployeePayslipApp.TestClasses
{
    [TestFixture]
    public class MonthlyIncomeTest
    {
        [TestCase]
        public void GetMonthlyIncome_20KSlab()
        {
            TaxSlabFactoryAbstract calculateMonthlyIncome = new TaxSlabConcrete();
            CalculateTaxFactory calculate = new CalculateTaxFactory();
            var slabType = string.Empty;
            decimal annualSalary = 6000;
            slabType = calculate.getTaxSlabType(annualSalary);
            Assert.AreEqual(500, calculateMonthlyIncome.GetMonthlyIncome(slabType));
        }

        [TestCase]
        public void GetMonthlyIncome_40KSlab()
        {
            TaxSlabFactoryAbstract calculateMonthlyIncome = new TaxSlabConcrete();
            CalculateTaxFactory calculate = new CalculateTaxFactory();
            var slabType = string.Empty;
            decimal annualSalary = 36000;
            slabType = calculate.getTaxSlabType(annualSalary);
            Assert.AreEqual(2833, calculateMonthlyIncome.GetMonthlyIncome(slabType));
        }

        [TestCase]
        public void GetMonthlyIncome_80KSlab()
        {
            TaxSlabFactoryAbstract calculateMonthlyIncome = new TaxSlabConcrete();
            CalculateTaxFactory calculate = new CalculateTaxFactory();
            var slabType = string.Empty;
            decimal annualSalary = 96000;
            slabType = calculate.getTaxSlabType(annualSalary);
            Assert.AreEqual(4667, calculateMonthlyIncome.GetMonthlyIncome(slabType));
        }

        [TestCase]
        public void GetMonthlyIncome_180KSlab()
        {
            TaxSlabFactoryAbstract calculateMonthlyIncome = new TaxSlabConcrete();
            CalculateTaxFactory calculate = new CalculateTaxFactory();
            var slabType = string.Empty;
            decimal annualSalary = 126000;
            slabType = calculate.getTaxSlabType(annualSalary);
            Assert.AreEqual(7167, calculateMonthlyIncome.GetMonthlyIncome(slabType));
        }

        [TestCase]
        public void GetMonthlyIncome_180KAboveSlab()
        {
            TaxSlabFactoryAbstract calculateMonthlyIncome = new TaxSlabConcrete();
            CalculateTaxFactory calculate = new CalculateTaxFactory();
            var slabType = string.Empty;
            decimal annualSalary = 1126000;
            slabType = calculate.getTaxSlabType(annualSalary);
            Assert.AreEqual(58966.666666666666666666666663, calculateMonthlyIncome.GetMonthlyIncome(slabType));
        }
    }
}
