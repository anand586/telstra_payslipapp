﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModelLayer.DataModels
{
    public class PayslipModel
    {
        public string EmployeeName { get; set; }
        public decimal AnnualSalary { get; set; }
        public decimal MonthlySalary { get; set; }
        public decimal MonthlyTax { get; set; }

    }
}
