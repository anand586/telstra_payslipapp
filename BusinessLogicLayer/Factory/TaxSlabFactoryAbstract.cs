﻿using BusinessLogicLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Factory
{
    public abstract class TaxSlabFactoryAbstract
    {
        public abstract IMonthlyIncome GetMonthlyIncome(string SlabType);
    }
}
