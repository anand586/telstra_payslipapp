﻿using BusinessLogicLayer.Classses;
using BusinessLogicLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Factory
{
    public class TaxSlabConcrete : TaxSlabFactoryAbstract
    {
        public override IMonthlyIncome GetMonthlyIncome(string SlabType)
        {
            switch (SlabType)
            {
                case "MonthlyIncome20kTaxSlab":
                    return new MonthlyIncome20kTaxSlab();

                case "MonthlyIncome40kTaxSlab":
                    return new MonthlyIncome40kTaxSlab();

                case "MonthlyIncome80kTaxSlab":
                    return new MonthlyIncome80kTaxSlab();

                case "MonthlyIncome180kTaxSlab":
                    return new MonthlyIncome180kTaxSlab();

                case "MonthlyIncome180kAndAboveTaxSlab":
                    return new MonthlyIncome180kAndAboveTaxSlab();

                default: throw new ApplicationException(string.Format("TaxSlab'{0}' not available", SlabType));
            }
        }
    }
}
