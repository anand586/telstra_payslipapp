﻿using BusinessLogicLayer.Constants;
using BusinessLogicLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Factory
{
    public class CalculateTaxFactory : ITaxSlabType
    {
        public string getTaxSlabType(decimal annualSalary)
        {
            string slabType = string.Empty;
            AppConstants constants = new AppConstants();

            if (annualSalary > 0 && annualSalary <= 20000)
            {
                slabType = constants.TAX_SLAB_20K;
            }
            else if (annualSalary > 20000 && annualSalary <= 40000)
            {
                slabType = constants.TAX_SLAB_40K;
            }
            else if (annualSalary > 40000 && annualSalary <= 80000)
            {
                slabType = constants.TAX_SLAB_80K;
            }
            else if (annualSalary > 80000 && annualSalary <= 180000)
            {
                slabType = constants.TAX_SLAB_180K; ;
            }
            else if (annualSalary > 180000)
            {
                slabType = constants.TAX_SLAB_ABOVE_180K;
            }
            return slabType;
        }
    }
}
