﻿using DataModelLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interfaces
{
    public interface IMonthlyIncome
    {
        public PayslipModel getMonthlyIncome(decimal annualSalary);
    }
}
