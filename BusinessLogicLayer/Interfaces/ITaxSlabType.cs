﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITaxSlabType
    {
        string getTaxSlabType(decimal annualSalary);
    }
}
