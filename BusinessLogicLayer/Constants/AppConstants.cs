﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Constants
{
    public class AppConstants
    {
        public string TAX_SLAB_20K = "MonthlyIncome20kTaxSlab";
        public string TAX_SLAB_40K = "MonthlyIncome40kTaxSlab";
        public string TAX_SLAB_80K = "MonthlyIncome80kTaxSlab";
        public string TAX_SLAB_180K = "MonthlyIncome180kTaxSlab";
        public string TAX_SLAB_ABOVE_180K = "MonthlyIncome180kAndAboveTaxSlab";
    }
}
