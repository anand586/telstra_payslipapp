﻿using BusinessLogicLayer.Interfaces;
using DataModelLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Classses
{
    public class MonthlyIncome180kAndAboveTaxSlab : IMonthlyIncome
    {
        public PayslipModel getMonthlyIncome(decimal annualSalary)
        {
            PayslipModel model = new PayslipModel();
            var taxableUpTo20k = 20000;
            var taxableAmount20KSlab = (taxableUpTo20k * 0.00m);

            var TaxableFrom20kTo40k = 20000;
            var taxableAmount40KSlab = (TaxableFrom20kTo40k * 0.10m);

            var taxable40kTo80k = 40000;
            var taxableAmount80KSlab = (taxable40kTo80k * 0.20m);

            var taxable80kTo180k = 100000;
            var taxableAmount180KSlab = (taxable80kTo180k * 0.30m);

            var taxable180kAbove = annualSalary - 180000;
            var taxableAmount180KAndAboveSlab = taxable180kAbove * 0.40m;

            var totalTax = taxableAmount20KSlab + taxableAmount40KSlab + taxableAmount80KSlab + taxableAmount180KSlab + taxableAmount180KAndAboveSlab;
            decimal grossMonthlyIncome = annualSalary / 12;

            model.MonthlyTax = totalTax / 12;
            model.MonthlySalary = grossMonthlyIncome - model.MonthlyTax;
            model.AnnualSalary = annualSalary;
            return model;
        }
    }
}
