﻿using BusinessLogicLayer.Interfaces;
using DataModelLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Classses
{
    public class MonthlyIncome20kTaxSlab : IMonthlyIncome
    {
        public PayslipModel getMonthlyIncome(decimal annualSalary)
        {
            PayslipModel model = new PayslipModel();
            model.MonthlyTax = 0.00m;
            model.MonthlySalary = annualSalary / 12;
            model.AnnualSalary = annualSalary;
            return model;
        }
    }
}
